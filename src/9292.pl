:- use_module(library(apply)).
:- use_module(library('http/http_client')).
:- use_module(library('http/json')).

:- dynamic(adress/2).

prettify("place", Id, Obj, Out) :-
	format(atom(Out), '~d: [~s ~s ~s] ~s\n', [Id, Obj.type, Obj.name, Obj.regionName, Obj.name]).
	
prettify("poi", Id, Obj, Out) :-
	format(atom(Out), '~d: [~s ~s ~s] ~s\n', [Id, Obj.poiType, Obj.place.name, Obj.place.regionName, Obj.name]).
	
prettify("stop", Id, Obj, Out) :-
	format(atom(Out), '~d: [~s ~s ~s] ~s\n', [Id, Obj.stopType, Obj.place.name, Obj.place.regionName, Obj.name]).
	
prettify("station", Id, Obj, Out) :-
	format(atom(Out), '~d: [~s ~s ~s] ~s\n', [Id, Obj.stationType, Obj.place.name, Obj.place.regionName, Obj.name]).
	
prettify(_, Id, Obj, Out) :-
	format(atom(Out), '~d: [~s] ~s\n', [Id, Obj.type, Obj.name]).
	
prettify([Id, Obj], Out) :-
	prettify(Obj.type, Id, Obj, Out).

prettifyJourney(Journey, Out) :-
	maplist(prettifyJourneyLeg, Journey.legs, LegList),
	atomic_list_concat(LegList, '', LegString),
	format(atom(Out), 'Departure: ~s, Arrival: ~s\n~s\n', [Journey.departure, Journey.arrival, LegString]).

prettifyJourneyLeg(Leg, Out) :-
	prettifyJourneyLeg(Leg.type, Leg, Out).

prettifyJourneyLeg("continuous", Leg, Out) :-
	format(atom(Out), '- ~s: ~s\n', [Leg.duration, Leg.mode.name]).

prettifyJourneyLeg("scheduled", Leg, Out) :-
	format(atom(Out), '- ~s: ~s ~s\n', [Leg.destination, Leg.mode.name, Leg.service]).

prettifyJourneyLeg(_, _, Out) :-
	format(atom(Out), 'Unkown\n', []).

getId(Obj, [IdNum, Obj]) :-
	nextId(IdNum), 
	setAdress(IdNum, Obj).
	
%get_time(TimeStamp), format_time(atom(FormattedTime), '%FT%H%M', TimeStamp), print(FormattedTime).
  
formattedTime(Timestamp, FormattedTime) :-
	format_time(atom(FormattedTime), '%FT%H%M', Timestamp).

now(Time) :- get_time(Time).

tomorrow(Time) :- now(T), Time is T + 24*60*60.	
	
%getStops(City, Stop) :-
%	format(atom(HREF), 'http://api.9292.nl/0.1/locations?lang=en-GB&q=~s&type=stop', [City]), 
%	http_get(HREF, Json, []), 
%	atom_json_term(Json, json(R), []), 
%	member('locations'=Locations, R), 
%	maplist(getId, Locations, Stop).
	
getLocations(City, LocationList) :-
	format(atom(HREF), 'http://api.9292.nl/0.1/locations?lang=en-GB&q=~s', [City]), 
	http_get(HREF, Json, []),
	atom_json_dict(Json, R, []), 
	maplist(getId, R.locations, LocationList).

getJourneys(From, To, Time, Result) :-
	formattedTime(Time, DepartureTime),
	format(atom(HREF), 'http://api.9292.nl/0.1/journeys?before=1&sequence=1&byFerry=true&bySubway=true&byBus=true&byTram=true&byTrain=true&lang=en-GB&from=~s&searchType=departure&interchangeTime=standard&after=5&to=~s&dateTime=~s', [From.id, To.id, DepartureTime]),
	http_get(HREF, Json, []),
	atom_json_dict(Json, R, []),
	get_dict(journeys, R, Result).

getAdress(VarName, Value):-
	adress(VarName,Value).

getAdress(VarName, []):-
	\+ adress(VarName, _).

%setAdress(VarName, Num) :-
%	number(VarName),
%	atom_number(Value, Num),
%	retractall(adress(VarName, _)),
%	asserta(adress(VarName, Value)).

setAdress(VarName, Value):-
	retractall(adress(VarName, _)),
	asserta(adress(VarName, Value)).

initIdGenerator() :-
	b_setval(idGen, 0).

nextId(Id) :-
	b_getval(idGen, Current),
	Id is Current + 1,
	b_setval(idGen, Id).
