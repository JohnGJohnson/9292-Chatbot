:- use_module(library(apply)).
:- use_module(library('http/http_client')).
:- use_module(library('http/json')).

temperature(City, Temp) :-
	format(atom(HREF), 'http://api.openweathermap.org/data/2.5/weather?q=~s&APPID=58bfc07a2a4406ff4a1c391bcd141306', [City]), 
	http_get(HREF, Json, []), 
	atom_json_term(Json, json(R), []), 
	member(main=json(W), R), 
	member(temp=T, W), 
	Temp is round(T - 273.15).
	
temperatureByCoords(LatLong, Temp) :-
	format(atom(HREF), 'http://api.openweathermap.org/data/2.5/weather?lat=~a&lon=~a&APPID=58bfc07a2a4406ff4a1c391bcd141306', [LatLong.lat, LatLong.long]),
	http_get(HREF, Json, []),
	atom_json_term(Json, json(R), []),
	member(main=json(W), R), 
	member(temp=T, W), 
	Temp is round(T - 273.15).