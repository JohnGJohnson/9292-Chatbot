% chatbot.pl

:- use_module(library(apply)).

:- consult(['alice', '9292', 'openweathermap']).

category([
	pattern([presentation, '.']), 
	template(['Welcome to the chatbot presentation by Fre-Meine Botter and Lukas Boonstra.'])
]).

%category([
%	pattern([next, '!']), 
%	that(['Welcome to the chatbot presentation by Fre-Meine Botter and Lukas Boonstra.']), 
%	template(['Der inhoud.'])
%]).

category([
	pattern([who, is, aswin, van, woudenberg, '?']), 
	template(['Aswin van Woudenberg', is, a, 'teacher', and, a, world, traveller, '.'])
]).

category([
	pattern([about, '.']), 
	template(['We made chatbot functionality for travelling by public transport.'])
]).

category([
	pattern([next, '!']), 
	that(['Welcome to the chatbot presentation by Fre-Meine Botter and Lukas Boonstra.']), 
	template(['We made chatbot functionality for travelling by public transport.'])
]).

category([
	pattern([star(_), new, functionality, star(_), '?']),
	template(['I am now capable of giving you information about public transportation and the weather at your destination.'])
]).

category([
	pattern([next, '!']), 
	that(['We made chatbot functionality for travelling by public transport.']), 
	template(['I am now capable of giving you information about public transportation and the weather at your destination.'])
]).

category([
	pattern([star(_),public,transportation, star(_), '.']),
	template(['You can give me a 2 adresses and a departure time I can you the possible journeys you can take.'])
]).

category([
	pattern([next, '!']),
	that(['I am now capable of giving you information about public transportation and the weather at your destination.']),
	template(['You can give me a 2 adresses and a departure time I can you the possible journeys you can take.'])
]).


category([
	pattern([star(_),example,star(_),journey, star(_),'?']),
	template(['Sure thing!. blabla addadress blabla addadress blabla datetime, after this you run the getJourney(from, to, time) command and it should give you possible journeys!'])
]).

category([
	pattern([search, for, star([Id]), '?']), 
	template([think(getAdress(Id, Location)),
		think(get_dict(name, Location, Name)),
		Id, is, Name])
]).

category([
	pattern([what, is, the, temperature, in, star([City]), '?']), 
	template([think(temperature(City, T)), 
		'The temperature in', City, 'is: ', T, '.'])
]).
	
category([
	pattern([temperature, at, star([Id]), '?']), 
	template([think(getAdress(Id, Location)),
		think(get_dict(name, Location, Name)),
		think(get_dict(latLong, Location, LatLong)),
		think(temperatureByCoords(LatLong, Temp)), 
		'The temperature at', Name, 'is', Temp, 'celsius.']),
	template(['Location',Id,'not found in database.'])
]).

category([
	pattern([my, star([Adress]), is, star([Id]), '.']),
	template([think(getAdress(Id, Loc)),
		think(setAdress(Adress, Loc)),
		think(get_dict(name, Loc, Pretty)),
		'Adress',Adress,'is now',Pretty,'.'])
]).

category([
	pattern([my, star([Adress]), is, in, star(Loc), '.']),
	template([think(atomic_list_concat(Loc, '+', Location)),
		think(getLocations(Location, Locations)),
		think(setAdress(lastAdress, Adress)),
		think(maplist(prettify, Locations, Pretty)),
		'Available locations are:\n', Pretty, 'Please give the number of your location, or say no.'])
]).

category([
	pattern([i, want, to, go, from, star([Start]), to, star([Destination]), '.']),
	template([think(now(Time)),
		think(getAdress(Start, StartLoc)),
		think(getAdress(Destination, DestinationLoc)),
		think(getJourneys(StartLoc, DestinationLoc, Time, Journeys)),
		think(nth0(0, Journeys, Journey)),
		think(prettifyJourney(Journey, Pretty)),
		Pretty])
]).

category([
	pattern([star([Id]), '.']),
	that(['Available locations are:', star(_), 'Please give the number of your location, or say no.']),
	template([think(getAdress(Id, Location)),
		think(getAdress(lastAdress, Adress)),
		think(setAdress(Adress, Location)),
		think(get_dict(name, Location, Name)),
		'The adress', Adress, 'is saved as', Name])
]).

category([
	pattern([no, '.']),
	that([star(_), 'Please give the number of your location, or say no.']),
	template(['That\'s unfortunate.'])
]).

category([
	pattern([what, is, the, temperature, at, star([Adress]), '?']),
	template([think(temperature(Adress, Temp)),
		'The temperature at', Adress ,is , Temp, 'celsius'
	])
]).


category([
	pattern(['yes.']), 
	that(['Do you want to play a game?']), 
	template(['No game!'])
]).

category([
	pattern(['no.']), 
	that(['Do you want to play a game?']), 
	template(['You afraid?'])
]).

% Todo
%category([
%	pattern([star(_), stops, star(_), in, star([City]), '?']), 
%	template([think(getStops(City, Stops)), think(maplist(prettify, Stops, Pretty)), 
%		'The', mainstops, in, City, 'are: \n', Pretty])
%]).

category([
	pattern([star(_), locations, star(_), in, star([City]), '?']), 
	template([think(getLocations(City, Locations)),
		think(maplist(prettify, Locations, Pretty)), 
		'The', locations, in, City, 'are: \n', Pretty])
]).

category([
	pattern([star(_)]), 
	template([random([
		['Do you want to play a game?']])
	])
]).

start() :-
	initIdGenerator(),
	write('Bot has started.'), nl,
	loop().

:- start.